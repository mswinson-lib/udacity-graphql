FROM ruby:latest

ADD . /var/services/udacity-graphql
WORKDIR /var/services/udacity-graphql

RUN ./scripts/setup
CMD ./scripts/init
