require 'bundler/setup'
require 'sinatra/cross_origin'
require 'graphql/schema/udacity'
require 'json'

require './app'
