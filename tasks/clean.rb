require 'fileutils'

desc 'clean'
task :clean do
  FileUtils.rm_rf('target/')
end
